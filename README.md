## GPRK

####Требуется установить

- рекоммендую использовать unix машину для деплоя
- git - любая версия
- node.js - v6.2.0 (http://nodejs.org)
- npm - 3.8.9 (идет вместе с node)
- mongodb - 2.6.7 (https://www.mongodb.com/download-center#community)
- forever (npm install -g forever)
- bower (npm install -g bower)

####Запуск

- после клонирования запустить `npm install` и `bower install`
- 
- запустить демон монго - `$> mongod`

> есть 2 варианта запуска сервера, используя `forever .` или `node .`, в отличии от node, forever запускает приложение в бэкграунде и перезапускает 
его если произошел крэш

- запуск через node `$> node server.js`
- остановить - `Ctrl + C`
- запуск через forever `$> forever start server.js`
- остановить - `$> forever stop server.js`
- в начале база пустая, что бы создать админа добавить префикс - `$> MONGO_SEED=true node server.js` или `$> MONGO_SEED=true forever start server.js`
- после запуска выполнить `$> forever list`, и скопировать путь лог файла, пример пути `/Users/my_account/.forever/JSio.log`
- открыть лог файл и взять сгенерированные учетные данные (логин/пароль от admin/user)
- что бы остановить сервер (в директории приложения выполнить) `$> forever stop server.js`


Вот так выглядит команда которуб я сам использую
> `NODE_ENV=production PORT=3000 MONGO_SEED=true LOG_FORMAT=tiny forever start server.js`


####Дополнительно

- запустить на своем порте - `$> PORT=80 + ${команда запуска}` - _порт 80 требует права sudo_
- добавить googleAnalytics - `$> GOOGLE_ANALYTICS_TRACKING_ID=1 + ${команда запуска}`
- добавтить удаленную базу данных mongo - `$> MONGOLAB_URI=mongodb://123.123.123.123:123/my-database`
- кастомный username админа - `$> MONGO_SEED_ADMIN_USERNAME=superadmin`
- кастомная почта админа - `$> MONGO_SEED_ADMIN_EMAIL=my@email.com`


####Recaptcha

https://www.google.com/recaptcha/admin#site/321063245?setup

    public 6LdNCSMTAAAAAJjEChmje0rz153yJzCRKmSCjNKi
    secret 6LdNCSMTAAAAADiEkXP4Au68fIaRCXtthlBMxc1Z
