(function () {
  'use strict';

  angular
    .module('articles.services')
    .factory('ArticlesService', ArticlesService);

  ArticlesService.$inject = ['$resource'];

  function ArticlesService($resource) {
    return $resource('api/articles/:articleId', {
      articleId: '@_id'
    }, {
      query: {
        url: 'api/articles',
        method: 'GET',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      enums: {
        method: 'GET',
        url: 'api/enums',
        isArray: false
      },
      addComment: {
        url: 'api/articles/:articleId/comment',
        method: 'POST',
        isArray: false
      },
      removeComment: {
        url: 'api/articles/:articleId/comment',
        method: 'DELETE',
        isArray: false
      }
    });
  }
}());
