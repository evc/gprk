(function () {
  'use strict';

  angular
    .module('articles')
    .controller('ArticlesListController', ArticlesListController);

  ArticlesListController.$inject = ['$stateParams', 'ArticlesService'];

  function ArticlesListController($stateParams, ArticlesService) {
    var vm = this;
    vm.currentPage = 1;
    vm.totalCount = 0;
    vm.itemsPerPage = 2;

    vm.enums = ArticlesService.enums();

    vm.pageChanged = function () {
      query();
    };


    function query() {
      ArticlesService.query({
        search: $stateParams.search,
        page_num: vm.currentPage,
        page_size: vm.itemsPerPage
      }, function (result) {
        vm.articles = result.articles;
        vm.totalCount = result.count;

        vm.categoryEntries = function (category) {
          return vm.articles
            .filter(function (item) {
              return item.category === category;
            })
            .map(function (item) {
              return item.entry;
            })
            .filter(function (value, index, self) {
              return self.indexOf(value) === index;
            });
        };
      });
    }

    query();
  }
}());
