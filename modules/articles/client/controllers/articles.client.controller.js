(function () {
  'use strict';

  angular
    .module('articles')
    .controller('ArticlesController', ArticlesController);

  ArticlesController.$inject = ['$scope', '$state', 'articleResolve', '$window', 'ArticlesService', 'Authentication', 'vcRecaptchaService'];

  function ArticlesController($scope, $state, article, $window, ArticlesService, Authentication, vcRecaptchaService) {
    var vm = this;

    vm.enums = ArticlesService.enums();
    vm.article = article;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.addComment = addComment;
    vm.removeComment = removeComment;
    vm.captcha = {};
    vm.widgetId = null;
    vm.response = null;
    vm.setWidgetId = function (widgetId) {
      vm.widgetId = widgetId;
    };

    if (vm.authentication.user && vm.authentication.user.roles.indexOf('user') > -1) {
      vm.authorized = true;
    }
    vm.setResponse = function (response) {
      vm.response = response;
    };
    vm.cbExpiration = function () {
      vm.response = null;
    };

    // Remove existing Article
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.article.$remove($state.go('articles.list'));
      }
    }

    // Save Article
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.articleForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.article._id) {
        vm.article.$update(successCallback, errorCallback);
      } else {
        vm.article.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('articles.view', {
          articleId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function addComment(isValid) {
      if (!vm.form.articleFormComment.captcha) {
        vm.error = 'Заполните каптчу';
      }
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.articleFormComment');
        return false;
      }
      vm.article.$addComment(vm.newComment, function (res) {
        vm.article = res;
        vm.newComment = {};
        vm.form.articleFormComment.$setPristine();
        vcRecaptchaService.reload();
      }, function (res) {
        vm.error = res.data.message;
      });
    }

    function removeComment(commentId) {
      var promptMsg = confirm('Вы уверены что хотите удалить комментарии?');
      if (promptMsg) {
        vm.article.$removeComment({
          commentId: commentId
        }, function (res) {
          vm.article = res;
        }, function (res) {
          vm.error = res.data.message;
        });
      }
    }
  }
}());
