(function () {
  'use strict';

  angular
    .module('articles')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // menuService.addMenuItem('topbar', {
    //   title: 'Статьи',
    //   state: 'articles.list',
    //   roles: ['*']
    // });

    menuService.addMenuItem('topbar', {
      title: 'Создать запись',
      state: 'articles.create',
      roles: ['user']
    });
  }
}());
