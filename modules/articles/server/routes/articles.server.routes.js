'use strict';

/**
 * Module dependencies
 */
var articlesPolicy = require('../policies/articles.server.policy'),
  articles = require('../controllers/articles.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/articles').all(articlesPolicy.isAllowed)
    .get(articles.list)
    .post(articles.create);

  app.route('/api/enums')
    .get(articles.enums);

  // Single article routes
  app.route('/api/articles/:articleId').all(articlesPolicy.isAllowed)
    .get(articles.read)
    .put(articles.update)
    .delete(articles.delete);

  app.route('/api/articles/:articleId/comment').all(articlesPolicy.isAllowed)
    .post(articles.addComment)
    .delete(articles.removeComment);

  app.route('/api/articles/:articleId/comment').all(articlesPolicy.isAllowed)
    .delete(articles.removeComment);

  // Finish by binding the article middleware
  app.param('articleId', articles.articleByID);
};
