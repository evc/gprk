'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  enums = require('./enum.server.model'),
  Schema = mongoose.Schema;

/**
 * Article Schema
 */
var zero = 0;
var ArticleSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  category: {
    type: String,
    enum: enums.categories
  },
  entry: {
    type: String
  },
  sub_paragraph: {
    type: Number
  },
  views: {
    type: Number,
    default: zero
  },
  content: {
    type: String,
    default: '',
    trim: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  comments: [{
    type: Schema.ObjectId,
    ref: 'Comment'
  }]
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

mongoose.model('Article', ArticleSchema);
