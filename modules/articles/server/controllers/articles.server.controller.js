'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  async = require('async'),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  Comment = mongoose.model('Comment'),
  Enums = require('../models/enum.server.model'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create new comment
 */
exports.addComment = function (req, res) {
  var comment = new Comment({
    name: req.query.name,
    content: req.query.content
  });

  var article = req.article;

  comment.save(function (err, done) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      article.update({
        $push: {
          comments: comment
        }
      }, function (updateErr) {
        if (updateErr) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(updateErr)
          });
        } else {
          article.comments.push(comment);
          res.json(article);
        }
      });
    }
  });
};

/**
 * remove comment
 */
exports.removeComment = function (req, res) {
  var article = req.article;

  console.log('_removeCommentId', req.query.commentId);
  var commentId = req.query.commentId;
  if (!commentId) {
    return res.status(400).send({
      message: 'Не найден комментарии'
    });
  } else {
    Comment.remove({
      _id: commentId
    }, function (fErr, fComment) {
      if (fErr) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(fErr)
        });
      } else {
        article.update({
          $pull: {
            comments: commentId
          }
        }, function (updateErr) {
          if (updateErr) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(updateErr)
            });
          } else {
            article.comments = article.comments.filter(function (item) {
              return item._id.toString() !== commentId;
            });
            res.json(article);
          }
        });
      }
    });
  }
};

/**
 * Create an article
 */
exports.create = function (req, res) {
  var article = new Article(req.body);
  article.user = req.user;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  var article = req.article;
  article.update({
    $inc: {
      views: 1
    }
  }, function () {
    res.json(article);
  });
};

/**
 * Update an article
 */
exports.update = function (req, res) {
  var article = req.article;

  article.title = req.body.title;
  article.content = req.body.content;
  article.category = req.body.category;
  article.entry = req.body.entry;
  article.sub_paragraph = req.body.sub_paragraph;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var article = req.article;

  article.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * List enumerators
 */
exports.enums = function (req, res) {
  res.json(Enums);
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  var search = req.query.search;
  var filter = {};
  if (search) {
    var reg = new RegExp(search, 'i');
    filter.$or = [{ title: { $regex: reg } }, { content: { $regex: reg } }];
  }
  var pagination = {
    page_num: req.query.page_num || 1,
    page_size: req.query.page_size || 10
  };

  async.parallel([function (callback) {
    Article.count(filter, callback);
  }, function (callback) {
    Article.find(filter)
      // .sort('-created')
      // .skip((pagination.page_num - 1) * pagination.page_size)
      // .limit(pagination.page_num * pagination.page_size)
      .populate('user', 'displayName')
      .populate('comments', 'name created')
      .lean()
      .exec(callback);
  }], function (err, results) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      var articles = results[1];
      articles.forEach(function (item) {
        item.content_short = item.content.substring(0, 200) + ' ...';
        item.comments.sort(function (a, b) {
          return b.created - a.created;
        });
        var last_comment = item.comments[0];
        item.last_commentator = last_comment ? last_comment.name : 'Пока нет комментариев';
      });
      res.json({
        articles: articles,
        count: results[0]
      });
    }
  });
};

/**
 * Article middleware
 */
exports.articleByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Article is invalid'
    });
  }

  Article.findById(id)
    .populate('user', 'displayName')
    .populate('comments', 'name content created')
    .exec(function (err, article) {
      if (err) {
        return next(err);
      } else if (!article) {
        return res.status(404).send({
          message: 'No article with that identifier has been found'
        });
      }
      req.article = article;
      next();
    });
};
